'use strict';

process.env.BABEL_ENV = 'production';
process.env.NODE_ENV = 'production';

// Makes the script crash on unhandled rejections instead of silently
// ignoring them. In the future, promise rejections that are not handled will
// terminate the Node.js process with a non-zero exit code.
process.on('unhandledRejection', err => {
  throw err;
});

// Ensure environment variables are read.
require('../config/env');

// Setup raven
const SENTRY_DSN = process.env.REACT_APP_SENTRY_SERVER_DSN;
const raven = require('raven');
raven.config(SENTRY_DSN).install();

const AWSXRay = require('aws-xray-sdk');
const express = require('express');
const basicAuth = require('express-basic-auth');
const cookiesMiddleware = require('universal-cookie-express');
const compression = require('compression');
const fs = require('fs');
const paths = require('../config/paths');
const checkRequiredFiles = require('react-dev-utils/checkRequiredFiles');

const render = require(paths.appBuild + '/server/server').default;
const PORT = parseInt(process.env.PORT, 10) || 3000;

const serverBuild = paths.appBuild + '/server';

if (!checkRequiredFiles([paths.appHtml, serverBuild + '/server.js'])) {
  process.exit(1);
}

let template = fs.readFileSync(paths.appBuild + '/index.html', 'utf8');
// Inject preload for main bundle
// TODO: This is a bit hacky. Move this to webpack or something
const mainJS = template.match(/\/static\/js\/main.[\w\d]*.js/g);
template = template.replace(
  '<head>',
  '<head>' +
    mainJS
      .map(js => '<link rel="preload" href="' + js + '" as="script">')
      .join('')
);

// @todo: since we are not using this, this could be used again when we get render to work
const loadableStats = JSON.parse(
  fs.readFileSync(paths.appBuild + '/client/react-loadable.json', 'utf8')
);
const app = express();

// Setup raven request handler before any other middleware
app.use(raven.requestHandler());

if (process.env.REACT_APP_XRAY_ENABLED && process.env.REACT_APP_XRAY_APP_NAME) {
  app.use(AWSXRay.express.openSegment(process.env.REACT_APP_XRAY_APP_NAME));
}

// Basic auth
if (process.env.BASIC_AUTH_USER) {
  app.use(
    basicAuth({
      users: {
        [process.env.BASIC_AUTH_USER]: process.env.BASIC_AUTH_PASS
      },
      challenge: true
    })
  );
}

app.use(cookiesMiddleware());
app.use(compression()); // gzip
app.use(express.static(paths.appBuild + '/client', {
  maxAge: process.env.STATIC_FILE_CACHE || '7d', // Increase this later on
  etag: false,
})); // serve static files

app.use(render(template, loadableStats));

// Setup raven error handler middleware before any other error handler
app.use(raven.errorHandler());


// Optional rendering of error page with sentry(raven) issue ID
/*
app.use((err, req, res, next) => {
  // The error id is attached to `res.sentry` to be returned
  // and optionally displayed to the user for support.
  res.statusCode = 500;
  res.end(res.sentry + '\n');
});
*/

app.listen(PORT, err => {
  if (err) {
    console.error(err);
    process.exit(1);
  } else {
    console.log('Listening to port', PORT);
  }
});

app.use(AWSXRay.express.closeSegment());
