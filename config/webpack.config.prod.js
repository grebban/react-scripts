const client = require('./webpack.config.client.prod');
const server = require('./webpack.config.server.prod');

module.exports = {
    client,
    server
};
